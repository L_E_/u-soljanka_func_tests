This test automation project contains 2 main parts: Java code(/src/test/java ...) and resources(/src/test/resources).

"/resources"
    Contains configuration and property files and data-sets as well. A data-set file is an DbUnit's XML data-set file used for data-base manipulations.

"/java"
    There are packages:
        "pages" - contains PageObjects for the AUT's pages;
        "tests" - automated tests themselves;
        "utils" - utility classes, helpers etc.

    "pages"
        BasePage is intended to be a super class for all PageObjects in the project. Contains common fields and methods like "logout()".

    "tests"
        Contain sub-packages named after corresponding functional areas.
        All test classes extend either AbstractTest(common pre-/post-conditions like launch/close browser) or AbstractAutoLoginTest(performs logging in as an administrator as a pre-condition step.)

    "utils"
        There is a number of sub-packages which naming corresponds to the utility services their content provide. Refer to clases' JavaDocs.

