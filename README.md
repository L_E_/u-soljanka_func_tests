# Проект автотестов, часть учебной "песочницы" по автоматизации тестирования.

```
#!java

Scroll down for **English** version.
```


О чём и для чего?
 Число желающих что-нибудь заавтоматизировать в тестировании растёт, а с чего начать и куда копать чаще всего неясно. Огромное количество платных тренингов порождает проблему выбора и не даёт уверенности в результатах. Данный проект это часть "самотренинга" - набора "выучись сам". Желающему предоставляются набор материалов для самостоятельного изучения, выполнения заданий и т.д. . Никаких разжёвываний и сверх-детальных объяснений, расчёт на инженерный подход, способность мыслить и самообучаться. Ожидается, что после выучивания инженер сможет участвовать в реальном проекте по автоматизации тестирования в качестве младшего специалиста.

 Самотренинг состоит из краткой теоретической части(конспекты), набора указаний для практики, а также простого веб-приложения для тестирования и зародыша проекта автоматизации на базе Selenium 2.0 WebDriver.

 Материалы(как текст, так и исходный код) предоставляются "как есть" и бесплатно для самостоятельного обучения. При цитировании или использовании в любых проектах по разработке ПО (открытых, коммерческих, некоммерческих и т.д.) - имейте совесть и ставьте ссылки на источник. Если есть желание попользоваться материалами как-то ещё или улучшить код - пишите в комментариях.

Предположения.
Изучающий - человек, желающий узнать что-то новое и готовый использовать данные материалы для этого.

Предполагается, что изучающий имеет/знает:
- 2-4 года опыта в ручном тестировании ПО;
- принципы и концепции тестирования;
- знания и опыт применения базовых техник тест-дизайна (классы эквивалентности, граничные значения и т.д.);
- начальные навыки в программировании. Желательно ООП.

Тестируемое приложение - У-Солянка(U_soljanka) = "Учебная солянка", .war-file лежит в разделе загрузок. Инструкции по настройке среды и разворачиванию приложения лежат в соответствующих разделах [Вики](https://bitbucket.org/L_E_/u-soljanka_func_tests/wiki). Конспект по автоматизации и дополнительные материалы лежат [здесь.](http://ojjjk.blogspot.com/2013/03/blog-post.html)

Проект авто-тестов состоит из основных частей: Java (/src/test/java ...) и ресурсы (/src/test/resources).

### /resources
Cодержит конфигурационные файлы и файлы свойств, а так же набор XML файлов используемых для наполнения БД.

### /java
Cодержит пакеты:

* pages - классы типа PageObject моделирующие страницы тестируемого приложения. Класс BasePage - предполагается как родительский для всех классов PageObject. Содержит общие поля и сервисы, такие как logout() или ссылки из меню сверху всех страниц;

* tests - собственно автотесты. Подпакеты названы в соответствии с тестируемыми функциональными областями приложения. Все классы тестов наследуются  либо от AbstractTest(общие пред-/пост-условия, например запуск/останов браузера) или AbstractAutoLoginTest(включает заход в систему под учёткой админа в качестве предусловия);

* utils - вспомогательные классы (доступ к БД, аналоги тестируемых сущностей и т.д.). Названия подпакетов соответствуют функциональному предназначению утилитных классов.

* * *

What this is about?
 There is a growing number of people interested in "automate something in my testing". However, there is little to nothing is clear about test automation starting point and moving direction. A hard choice between numerous trainings entails frustration and gives no hope of good results. This project is a part of a "self-training" - a kind of "Learn-It-Yourself" set. There are materials and artefacts to help an interested person to get knowledge and skills in test automation and get those by yourself. Important: no detailed explanations, the plan is to use your own brain - you are an engineer! I hope that a self-trained person would be able to participate in a real test automation project as a junior engineer.

The self-training materials are: a small set of theoretical "papers" (in Russian only so far) and specially designed web-application + automated tests for it.

All the materials are provided "as is" and and without any charge for personal/self-educational purposes. If you are to use any part of the self-training in your own personal or commercial software development project, posts, articles, papers etc - be honest and refer to the source.
If you are willing to participate or would like to use the materials somehow- feel free to contact me via comments.

Assumptions.
A learner has/knows:
- 2-4 years of manual software testing experience;
- testing theory;
- basic test design techniques;
- basic programming skills (OOP, Java).

Application-under-test: U_soljanka, the .war-file can be found on Downloads page. Environment set-up instructions are located in the [Wiki](https://bitbucket.org/L_E_/u-soljanka_func_tests/wiki). Short notes on test automation theory are [here.](http://ojjjk.blogspot.com/2013/03/blog-post.html)

This test automation project contains 2 main parts: Java code(/src/test/java ...) and resources(/src/test/resources).

### /resources
Contains configuration and property files and data-sets as well. A data-set file is an DbUnit's XML data-set file used for data-base manipulations.

### /java
There are packages:

* pages - contains PageObjects for the AUT's pages. BasePage is intended to be a super class for all PageObjects in the project. Contains common fields and methods like "logout()".;

* tests - automated tests themselves. Contain sub-packages named after corresponding functional areas. All test classes extend either AbstractTest(common pre-/post-conditions like launch/close browser) or AbstractAutoLoginTest(performs logging in as an administrator as a pre-condition step.);

* utils - utility classes, helpers etc. There is a number of sub-packages which naming corresponds to the utility services their content provide. Refer to classes' JavaDocs.