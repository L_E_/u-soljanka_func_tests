package le.u_solnka.utils;

import le.u_solnka.utils.tools.Log;
import org.jboss.resteasy.client.ClientRequest;
import org.jboss.resteasy.client.ClientResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Base class for RESTful web service related tests.<br/>
 *
 * See http://www.ibm.com/developerworks/library/ws-restful/  and http://yandex.ua/yandsearch?text=restful%20web%20services&lr=143 for more detail on RESTful WS.
 *
 * @author L.E.
 * @since 2013-11-15
 */
public abstract class AbstractRestTest {

    private static final Logger logger = LoggerFactory.getLogger(AbstractRestTest.class);
    /**
     * Children must implement this method.
     * Shall return resource-under-test URL, e.g for "Calc" resource - localhost:8080/aut/rest/v1/calc
     */
    protected abstract String getResourceURL();

    /**
     * Calls HTTP-GET methods foe resource and query params specified.
     * More on HTTP methods http://www.w3.org/Protocols/rfc2616/rfc2616-sec9
     * @param queryParams - query parameters to use.
     *
     * @return - the call's response.
     */
    protected ClientResponse callGET(String queryParams) {
        String requestURL = getRequestURL(queryParams);
        ClientRequest request = new ClientRequest(requestURL);

        //TODO: Use whatever would be useful.
        //request.accept("application/json");
        //request.accept("application/xml");

        //TODO: Cast to your own type-under-test, not to String if needed.
        ClientResponse<String> response = null;

        try {
            logger.debug("Attempting to call HTTP-GET for " + requestURL);
            response = request.get(String.class);
        } catch (Exception e) {
            Log.logException("Exception upon calling HTTP-GET for " + getRequestURL(queryParams), e);
        }

        logger.debug("HTTP-GET, response: " + response);

        return response;
    }

    private String getRequestURL(String queryParams) {
        String params =  queryParams.startsWith("?") ? queryParams : "?" + queryParams;

        return "http://" + getResourceURL() + params;
    }
}
