package le.u_solnka.utils.data;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A container for configurable data: user names, passwords etc.
 * Spring bean is used, see func_tests.config.xml.
 */
public class DataHolder {
    private static final Logger logger = LoggerFactory.getLogger(DataHolder.class);

    private static DataHolder holder;

    private DataHolder(){}

    public static DataHolder getHolder() {
        // Generally Spring based approach might be an over-head, you can use simple PropertiesHolder to get stored values.
        if (null == holder) {
            try {
                holder = (DataHolder) AppContext.getContext().getBean("data");
            } catch (Exception e) {
                logger.error("Cannot init Data Holder.", e);
            }
        }

        return holder;
    }

    private String adminName;

    private String adminPassword;

    public String getAdminName() {
        return adminName;
    }

    public void setAdminName(String adminName) {
        this.adminName = adminName;
    }

    public String getAdminPassword() {
        return adminPassword;
    }

    public void setAdminPassword(String adminPassword) {
        this.adminPassword = adminPassword;
    }
}