package le.u_solnka.utils.data;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A container for all AUT's page URLs.
 * Spring bean is used, see func_tests.config.xml.
 */
public class URLsHolder {
    private static final Logger logger = LoggerFactory.getLogger(URLsHolder.class);

    private static URLsHolder holder;

    private String hostUrl;

    private String hostPort;

    private String baseUrl;

    private String pageMain;

    private String pageHome;

    private String pageUsersView;

    private String pageUserCreate;

    private String pageUserViewEdit;

    private String restCalc;

    private String restVersion;

    private URLsHolder(){}

    public static URLsHolder getHolder() {
        // Generally Spring based approach might be an over-head, you can use simple PropertiesHolder to get stored values.
        if (null == holder){
            try {
                holder = (URLsHolder) AppContext.getContext().getBean("urls");
            } catch (Exception e) {
                logger.error("Cannot init URLs Holder.", e);
            }
        }

        return holder;
    }

    public String getHostUrl() {
        return hostUrl;
    }

    public void setHostUrl(String hostUrl) {
        this.hostUrl = hostUrl;
    }

    public String getHostPort() {
        return hostPort;
    }

    public void setHostPort(String hostPort) {
        this.hostPort = hostPort;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getPageMain() {
        return pageMain;
    }

    public void setPageMain(String pageMain) {
        this.pageMain = pageMain;
    }

    public String getPageHome() {
        return pageHome;
    }

    public void setPageHome(String pageHome) {
        this.pageHome = pageHome;
    }

    public String getPageUsersView() {
        return pageUsersView;
    }

    public void setPageUsersView(String pageUsersView) {
        this.pageUsersView = pageUsersView;
    }

    public String getPageUserCreate() {
        return pageUserCreate;
    }

    public void setPageUserCreate(String pageUserCreate) {
        this.pageUserCreate = pageUserCreate;
    }

    public String getPageUserViewEdit() {
        return pageUserViewEdit;
    }

    public void setPageUserViewEdit(String pageUserViewEdit) {
        this.pageUserViewEdit = pageUserViewEdit;
    }

    public void setRestCalc(String restCalc) {
        this.restCalc = restCalc;
    }

    public String getRestCalc() {
        return restCalc;
    }

    public void setRestVersion(String restVersion) {
        this.restVersion = restVersion;
    }

    public String getRestVersion() {
        return restVersion;
    }
}