package le.u_solnka.utils.db;

import le.u_solnka.utils.data.AppContext;
import le.u_solnka.utils.tools.Log;
import org.dbunit.DatabaseUnitException;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ReplacementDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.ext.mysql.MySqlConnection;
import org.dbunit.operation.DatabaseOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import javax.sql.DataSource;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * DB utility based on DBUnit.
 *
 */
public class DbUtil {
    private static final Logger logger = LoggerFactory.getLogger(DbUtil.class);

    public static final String DATASETS_DIRECTORY = "/datasets";

    @Value( "${db.schema}" )
    private static String schema;

    private static IDatabaseConnection connection;

    public static DbUtil instance;

    public static DbUtil getInstance() {
        if (null == instance){
            instance = new DbUtil();
        }

        return instance;
    }

    /**
     * Deletes all data in the tables specified by the special data-set.xml.
     *
     * @throws Exception
     */
    public void cleanAll() {
        execute(DatabaseOperation.DELETE_ALL, getDataSet("clean_all.xml"));
    }

    /**
     * Inserts data in the User table. See users.xml data-set for data details.
     *
     * @throws Exception
     */
    public void insertStandardUserEntries() {
        execute(DatabaseOperation.CLEAN_INSERT, getDataSet("users.xml"));
    }

    private synchronized static IDatabaseConnection getConnection() {
        if (null == connection){
            DataSource dataSource = (DataSource) AppContext.getContext().getBean("dataSource");

            try {
                Connection sqlConnection = dataSource.getConnection();
                connection = new MySqlConnection(sqlConnection, schema);
                connection.getConfig().setProperty("http://www.dbunit.org/features/batchedStatements", true);
            } catch (DatabaseUnitException e) {
                logger.error("Error getting connection to MySql.", e);
            }catch (SQLException sqlEx) {
                logger.error("Error getting connection using data-source.", sqlEx);
            }
        }

        return connection;
    }

    private IDataSet getDataSet(String datasetFileName) {
        InputStream inpt = getClass().getResourceAsStream(DATASETS_DIRECTORY + "/" + datasetFileName);

        if (null == inpt) {
            String msg = "Was not able to find the data-set file " + datasetFileName;
            Log.logError(msg);
            throw new RuntimeException(msg);
        }

        //NOTE: //http://dbunit.996259.n3.nabble.com/skipping-missing-columns-during-insert-instead-of-sending-null-td723.html issue.
        ReplacementDataSet dataSet = null;
        try {
            dataSet = new ReplacementDataSet(new FlatXmlDataSetBuilder().build(inpt));
        } catch (DataSetException dse) {
            Log.logException("DataSet error.", dse);
            throw new RuntimeException(dse);
        }
        dataSet.addReplacementObject("[NULL]", null);

        return dataSet;
    }

    /**
     * Executes the specified DBUnit operation against the dataset.<br>
     *
     * @param operation - DB operation to perform.
     *
     * @param dataSet - data-set to use.
     *
     * @return True in case of success. False otherwise.
     */
    private boolean execute(DatabaseOperation operation, IDataSet dataSet) {
        boolean result = false;
        try{
            operation.execute(getConnection(), dataSet);
            result = true;
        }catch (SQLException se) {
            logger.error("SQL error." + se.getMessage());
            SQLException exNext = se.getNextException();

            if (exNext != null) {
                logger.error("SQL error." + exNext.getMessage());
                throw new RuntimeException(exNext);
            }

        }catch (Exception e) {
            logger.error("SQL error." + e.getMessage());
            throw new RuntimeException(e);
        }

        return result;
    }
}