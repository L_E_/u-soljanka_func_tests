package le.u_solnka.utils;

import le.u_solnka.utils.data.URLsHolder;
import le.u_solnka.utils.db.DbUtil;
import le.u_solnka.utils.helpers.Nav;
import le.u_solnka.utils.tools.Log;
import le.u_solnka.utils.tools.TestListener;
import org.testng.annotations.*;

/**
 * Base class for GUI tests. Contains common pre-/post-conditions steps and auxiliary methods.<br/>
 *
 * TODO: use listeners to track test failures like IOException. <br/>
 * TODO: log additional info like IP etc before test start.     <br/>
 */
@Listeners (TestListener.class)
public abstract class AbstractTest {

    @BeforeClass
    public void initClass(){
        Browser.getBrowser();
    }

	/**
	 * Performs precondition action(s) before each test start.
	 * <p>
	 */
	@BeforeMethod
	public void initTest() {
        Log.logInfo("Cleaning up the DB.");
        DbUtil.getInstance().cleanAll();

        Log.logInfo("Getting to Main page.");
        Nav.toURL(URLsHolder.getHolder().getPageMain());

        Log.logTestStart();
	}

	/**
	 * Performs post-condition action(s) after each test finish.
	 * <p>
	 */
    @AfterMethod
    public void tearDownTest(){
        Nav.logout();
    }

	@AfterClass
	public void tearDownClass() {
		//Close the browser
        Browser.quit();
	}
}