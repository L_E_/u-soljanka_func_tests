package le.u_solnka.utils.tools;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.internal.TestResult;

/**
 * An extension for TestNG's Reporter.
 * Adds more convenient logging interface.
 *
 * TODO: Add screen-shots capturing.
 * TODO: Add "multi errors" support, see http://seleniumexamples.com/blog/guide/using-soft-assertions-in-testng/
 */
public class Log extends Reporter {
    private static final Logger logger = LoggerFactory.getLogger(Log.class);

	private static String newLine = "\r\n </br>";

	/**
	 * Logs verification error commented by the message specified.
	 * @param errorMessage - error's comment.
	 */
	public static void logVerificationError(String errorMessage){
		logVerificationError(errorMessage, null);
	}

	/**
	 * Logs verification error commented by the message specified.
	 * @param errorMessage - error's comment.
	 * @param throwable - The throwable to log.
	 */
	public static void logVerificationError(String errorMessage, Throwable throwable) {
		log(String.format("%s %s [VERIFICATION ERROR] %s", newLine, Util.getTimeStampForLog(), errorMessage), false);
        reportFailure(errorMessage, throwable);
	}

    /**
	 * Logs Exception commented by the message specified.
	 * @param message - the comment.
	 * @param throwable - The throwable to log.
	 */
	public static void logException(String message, Throwable throwable) {
        logMessage(String.format("[Exception] %s", message));
        reportFailure(message, throwable);
	}

    /**
     * Logs Error commented by the message specified.
     * @param message - the comment.
     * @param throwable - The throwable to log.
     */
    public static void logError(String message, Throwable throwable) {
        logMessage(String.format("[ERROR] %s", message));
        reportFailure(message, throwable);
    }

    /**
     * Logs Error commented by the message specified.
     */
    public static void logError(String message){
        logError(message, null);
    }
	
	/**
	 * Logs test step message.
	 * @param stepMessage - test step's comment.
	 */
	public static void logStep(String stepMessage){
		logMessage(String.format("[Step] %s", stepMessage));
	}
	
	/**
	 * Logs test Info message.
	 * @param message - Info message to log.
	 */
	public static void logInfo(String message){
		logMessage(String.format("[Info] %s", message));
	}
	
	/**
	 * Logs test Warning message.
	 * @param message - Warning message to log.
	 */
	public static void logWarning(String message){
		logMessage(String.format("[WARNING] %s", message));
	}

    /**
     * Logs test "New test start" message.
     */
    public static void logTestStart(){
        logMessage(newLine + "=============> [Starting new test]");
    }

    /**
     * Logs DEBUG captured message to the log.
     * @param message - DEBUG message to log.
     */
    public static void logDebugMessage(String message){
        logMessage(String.format("---> [DEBUG] %s", message));
    }
	
	/**
	 * Logs test message.
	 * @param message - message to log.
	 */
	public static void logMessage(String message) {
		log(String.format("%s %s %s", newLine, Util.getTimeStampForLog(), message), false);
        logger.info(message);
	}

	/**
	 * Captures screen-shot and saves it to the specified location.
	 */
	public void captureScreenshot() {
        //TODO: implement screen-shots capturing.
        throw new UnsupportedOperationException("Not implemented yet");
	}

    private static void reportFailure(String message, Throwable throwable) {
        logger.error(message);

        TestResult tr = (TestResult) Reporter.getCurrentTestResult();

        tr.setStatus(ITestResult.FAILURE);
        if (null != throwable){
            tr.setThrowable(throwable);
        }

        Reporter.setCurrentTestResult(tr);
    }
}