package le.u_solnka.utils;

import le.u_solnka.utils.helpers.Nav;
import org.testng.annotations.BeforeMethod;

/**
 * Base test class with auto-logging in as an admin before actual testing.
 *
 */
public abstract class AbstractAutoLoginTest extends AbstractTest {

	/**
	 * Performs logging in as admin before test start.
	 */
	@BeforeMethod
	public void loginAsAdminBeforeTest(){
        Nav.loginAsAdmin();
	}
}