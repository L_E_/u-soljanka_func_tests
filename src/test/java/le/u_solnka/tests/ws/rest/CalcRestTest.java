package le.u_solnka.tests.ws.rest;

import le.u_solnka.utils.AbstractRestTest;
import le.u_solnka.utils.data.URLsHolder;
import le.u_solnka.utils.tools.Log;
import le.u_solnka.utils.tools.Verify;
import org.apache.commons.httpclient.HttpStatus;
import org.jboss.resteasy.client.ClientResponse;
import org.testng.annotations.Test;

/**
 * A container for Calculator REST resource related tests.<br/>
 *
 * @author L.E.
 * @since 2013-11-15
 */
public class CalcRestTest extends AbstractRestTest {

    /**
     * Gets URL for the REST resource-under-test.
     *
     * @return - URL for the REST resource-under-test.
     */
    @Override
    protected String getResourceURL() {
        return URLsHolder.getHolder().getRestCalc();
    }

    /**
     * The tests verifies whether it is possible to ge sum of two real values using the AUT's Web Service..
     * <p>
     * Requirements:
     * <ul>
     *     <li>9.1</li>
     *     <li>9.2 ("+" related part)</li>
     *     <li>9.4</li>
     * </ul>
     * </p>
     * <p>
     * Scenario:
     * <ul>
     *     <li>Set-up HTTP-GET call(specify values for query params);
     *     <li>Call "HTTP-GET";
     *     <li>{Verify} Response HTTP STATUS is OK(200);
     *     <li>{Verify} Response contains correct result value (sum of two real values);
     * </ul>
     * </p>
     */
    @Test(groups={"rest"}, description="Test 'addition via WS'.")
    public void testCalculator_Add() {
        //TODO: Use data-driven approach.
        //TODO: Use an enum for math operations.
        //TODO: provide query params builder.
        Log.logStep("Set-up HTTP-GET call(specify values for query params)");
        String queryParams = "operation=add&term1=2.78&term2=3.14";

        //TODO Check for null.
        Log.logStep("Calling HTTP-GET for Calc resource with following query params: " + queryParams);
        ClientResponse response = callGET(queryParams);

        Log.logStep("{Verify}Response HTTP STATUS is OK(200)");
        Verify.Equals(HttpStatus.SC_OK, response.getStatus(), "Unexpected HTTP Response Status.");

        Log.logMessage("Response entity: \n" + response.getEntity()); //Just out of the curiosity.
        Log.logStep("{Verify} Response contains correct result value (sum of two real values)");
        //TODO:Complete this.
    }
}
