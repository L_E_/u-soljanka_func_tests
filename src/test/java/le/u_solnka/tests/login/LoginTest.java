package le.u_solnka.tests.login;

import le.u_solnka.pages.HomePage;
import le.u_solnka.pages.LoginPage;
import le.u_solnka.utils.AbstractTest;
import le.u_solnka.utils.Browser;
import le.u_solnka.utils.data.URLsHolder;
import le.u_solnka.utils.tools.Log;
import le.u_solnka.utils.tools.Verify;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

/**
 * A container class for login/logout related methods.<br/>
 *
 * @author L.E.
 * @since 2012-11-25
 */
public class LoginTest extends AbstractTest {

    /**
     * Performs logging in using valid admin credentials.
     * Checks whether login successful.
     * Performs logout.
     * Checks whether logout successful.<br/>
     * Note: The DB is cleaned up in parent's @BeforeMethod.
     * <p>
     * Requirements:
     * <ul>
     *     <li>1.1</li>
     *     <li>1.6</li>
     * </ul>
     * </p>
     * <p>
     * Scenario:
     * <ul>
     *     <li>Open AUT Index/Login page (is performed in @see AbstractTest);
     *     <li>Init Login Page model;
     *     <li>Type in valid name/password and submit the form;
     *     <li>{Verify} Current page is the Home page;
     *     <li>Click LogOut;
     *     <li>{Verify} Current page is Index/Login page;
     * </ul>
     * </p>
     */
    @Test(groups={"login"}, description="Login/logout with  valid credentials")
    public void testLoginAsAdminLogout() {
        //See http://code.google.com/p/selenium/wiki/PageFactory for PageFactory description.
        LoginPage loginPage = PageFactory.initElements(Browser.getDriver(), LoginPage.class);

        Log.logStep("Login ");
        //TODO: username/pwd should be specified as properties. Or use Spring beans for that.
        HomePage homePage = loginPage.loginAs("admin", "123qwe");

        Log.logStep("{Verify} Current page is the Home page;");
        String expectedURL = URLsHolder.getHolder().getPageHome();
        Verify.isCurrentUrlContainsExpected(expectedURL, "Wrong URL after logging in.");

        Log.logStep("Click LogOut;");
        loginPage = homePage.logout();

        Log.logStep("{Verify} Current page is Index/Login page;");
        Verify.True(
                loginPage.isLoginNameFieldPresent(),
                "There is no LoginName field, probably current page is not Login/Index");

        Verify.True(
                loginPage.isPasswordFieldPresent(),
                "There is no Password field, probably current page is not Login/Index");
    }
}