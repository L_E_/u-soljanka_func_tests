package le.u_solnka.tests.user;

import le.u_solnka.pages.CreateUserPage;
import le.u_solnka.pages.HomePage;
import le.u_solnka.utils.AbstractAutoLoginTest;
import le.u_solnka.utils.data.URLsHolder;
import le.u_solnka.utils.entities.TestUser;
import le.u_solnka.utils.tools.Log;
import le.u_solnka.utils.tools.Util;
import le.u_solnka.utils.tools.Verify;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * A container for User entity related tests.<br/>
 *
 * @author L.E.
 * @since 2013-08-06
 */
public class UserTest extends AbstractAutoLoginTest {

    /**
     * The tests verifies whether it is possible to add a new user from Home page.
     * Note: only required fields are filled in.
     * //TODO: Complete the task (fill in all the fields etc), update Requirements section and remove "enabled = false" after that.
     * <p>
     * Requirements:
     * <ul>
     *     <li>2.1</li>
     *     <li>2.4.2</li>
     *     <li>2.4.3</li>
     * </ul>
     * </p>
     * <p>
     * Scenario:
     * <ul>
     *     <li>Clean up th DB(no users except un-countable 'admin');
     *     <li>Login as admin;
     *     <li>Click "Add new user" on Home page;
     *     <li>Fill in the form(only required fields) and submit the form;
     *     <li>{Verify} Total users count has been increased (+1);
     *     <li>{Verify} There is corresponding record in the Users table;
     *     <li>Click to view the record;
     *     <li>{Verify} the form shows saved new user's data.
     * </ul>
     * </p>
     *
     * [ NOTE: ] ("enabled = false" below means that this test won't be executed even if you explicitly point to it since the test is ignored.)
     */
    @Test(enabled = false, groups={"user"}, description="Add new user from Home page.")
    public void testAddNewUserFromHomePage_OnlyRequiredFields() {
        //DB clean-up and logging as an admin is made in base class(es) in @BeforeMethod method(s).

        Log.logStep("Click 'Add new user' on Home page;");

        //Assuming pre-condition methods worked well, we are on the "Home" page at the moment.
        //However, let's check one more time. //TODO: Add a corresponding Assert here.
        Verify.isCurrentUrlContainsExpected(URLsHolder.getHolder().getPageHome(), "Current page is not 'Home'");
        CreateUserPage createUserPage = new HomePage().gotoCreateNewUser();

        //Create and fill in "user-to-create" object.
        TestUser userToCreate = new TestUser(
                Util.getSomeVeryShortValue(),
                Util.getSomeVeryShortValue(),
                Util.getSomeShortValue(),
                Util.getSomeValue(),
                "2001-01-14");
        Log.logInfo("User-to-create: " + userToCreate);

        Log.logStep("Fill in the form(only required fields) and submit the form;");
        createUserPage.fillInTheForm(userToCreate);

        createUserPage.submit();

        Assert.fail("//TODO: Finish this.");
        //TODO: Complete the scenario:
        /** <li>{Verify} Total users count has been increased (+1);
        * <li>{Verify} There is corresponding record in the Users table;
        * <li> Click to view the record;
        * <li>{Verify} the form shows saved new user's data.*/
    }
}