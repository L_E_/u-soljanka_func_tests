package le.u_solnka.pages;

import le.u_solnka.utils.Browser;
import le.u_solnka.utils.helpers.UiHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * PageObject class for "Login" page.<br/>
 * Partially uses <a href="http://code.google.com/p/selenium/wiki/PageFactory">PageFactory</a>.
 *
 * NOTE: the class is generally not optimal for a production usage and needs refactoring. See the comments.
 */
public class LoginPage extends BasePage {
    private static final Logger logger = LoggerFactory.getLogger(LoginPage.class);

	public LoginPage(WebDriver driver) {
		super(driver);
	}

    // using = "loginName" - this is OK till that ID is only needed here, at 1 place.
    // we have a violation of that here(see at teh bottom), so - need refactoring and optimization.
    @FindBy(how = How.ID, using = "loginName")
    private WebElement tbLoginName;

    @FindBy(how = How.ID, using = "psswd")
    private WebElement tbPassword;

    /**
     * Types in loginName and password and submits the login form.
     * @param loginName - name to use for login.
     * @param password - password to use.
     * @return - HomePage PageObject.
     */
    public HomePage loginAs(String loginName, String password) {
        logger.debug("Logging as {}/{}", loginName, password);

        UiHelper.sendKeys(tbLoginName, loginName);
        UiHelper.sendKeys(tbPassword, password);
        submit();

        return PageFactory.initElements(Browser.getDriver(), HomePage.class);
    }

    /**
     * Checks whether Login Name field present on current page.
     *
     * @return True if it is.
     */
    public boolean isLoginNameFieldPresent(){
        // Generally this is bad practice to duplicate literals ("loginName" here and at the top).
        // TODO: provide waitForPresence(WebElement element) method and use it in such cases.
        return UiHelper.isPresent(By.id("loginName"));
    }

    /**
     * Checks whether Password field present on current page.
     *
     * @return True if it is.
     */
    public boolean isPasswordFieldPresent(){
        // See the comment for £isLoginNameFieldPresent()
        // TODO: provide waitForPresence(WebElement element) method and use it in such cases.
        return UiHelper.isPresent(By.id("psswd"));
    }
}