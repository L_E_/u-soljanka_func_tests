package le.u_solnka.pages;

import le.u_solnka.utils.helpers.UiHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * PageObject class for "Home" page.
 * //TODO: PageFactory (<a href="http://code.google.com/p/selenium/wiki/PageFactory">PageFactory</a>) may be an option.
 */
public class HomePage extends BasePage {
    private static final Logger logger = LoggerFactory.getLogger(HomePage.class);

    /**
     * ById locator for LoggedUser field.
     */
    private static final By BY_TB_LOGGED_USER = new By.ById("tbLoggedUser");

    /**
     * ById locator for "To View Users" link.
     */
    private static final By BY_LNK_USERS = new By.ById("users");

    /**
     * ById locator for "To View Addresses" link.
     */
    private static final By BY_LNK_ADDRESSES = new By.ById("addresses");

    /**
     * ById locator for "To View Authors" link.
     */
    private static final By BY_LNK_AUTHORS = new By.ById("authors");

    /**
     * ById locator for "To View Books" link.
     */
    private static final By BY_LNK_BOOKS = new By.ById("books");

    /**
     * ById locator for "Add New User" link.
     */
    private static final By BY_LNK_NEW_USER = new By.ById("newUser");

    /**
     * ById locator for "Add New Address" link.
     */
    private static final By BY_LNK_NEW_ADDRESS = new By.ById("newAddress");

    /**
     * ById locator for "Add New Author" link.
     */
    private static final By BY_LNK_NEW_AUTHOR = new By.ById("newAuthor");

    /**
     * ById locator for "Add New Book" link.
     */
    private static final By BY_LNK_NEW_BOOK = new By.ById("newBook");

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public HomePage() {
    }

    /**
     * Clicks "Add new user" link under Actions.
     *
     * @return - CreateUserPage PageObject.
     */
    public CreateUserPage gotoCreateNewUser() {
        logger.debug("Navigating to CreateNewUser page from Home page.");

        UiHelper.click(BY_LNK_NEW_USER);

        return new CreateUserPage();
    }

    /**
     * Checks whether Login Name field present on current page.
     *
     * @return True if it is.
     */
    public boolean isLoggedUserFieldPresent(){
        return UiHelper.isPresent(BY_TB_LOGGED_USER);
    }

    /**
     * Gets text value of the LoggedUser text-box.
     *
     * @return text value of the LoggedUser text-box.
     */
    public String getLoggedUserName(){
        return UiHelper.getText(BY_TB_LOGGED_USER);
    }
}