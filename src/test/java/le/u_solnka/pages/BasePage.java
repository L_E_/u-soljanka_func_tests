package le.u_solnka.pages;

import le.u_solnka.utils.Browser;
import le.u_solnka.utils.helpers.UiHelper;
import le.u_solnka.utils.tools.Log;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Base class for all <a href="http://code.google.com/p/selenium/wiki/PageObjects">PageObjects</a>.
 *
 * //TODO: PageFactory (<a href="http://code.google.com/p/selenium/wiki/PageFactory">PageFactory</a>) may be an option.
 */
public class BasePage {
	protected WebDriver driver;

    /**
     * "Log out link" locator. Note, there is another way to locate elements - using @FindBy annotations.
     */
    protected static final By BY_LNK_LOGOUT = new By.ById("logOut");

    /**
     * ByClassName locator for [Submit] button.
     * The button has the same class, but different captions on different forms.
     */
    protected static final By BY_BTN_SUBMIT = new By.ByClassName("t-beaneditor-submit");

    /**
     * ByName locator for [Cancel] button.
     */
    protected static final By BY_BTN_CANCEL = new By.ByName("cancel");

    /**
     * Locator for HOME link.
     */
    protected static final By BY_LNK_HOME = new By.ById("to_page_Home");

	/**
	 * Default constructor. Need it to use <a href="http://selenium.googlecode.com/svn/trunk/docs/api/java/index.html">PageFactory</a>
	 * @param driver - current WebDriver(browser) object.
	 */
	public BasePage(WebDriver driver) {
		this.driver = driver;
	}

    /**
     * An alternative constructor, allows not to use PageFactory.
     */
    public BasePage(){
    }

    /**
     * Submits current page/form using the [SUBMIT]
     */
    public void submit(){
        UiHelper.submit(BY_BTN_SUBMIT);
    }

    /**
     * Clicks logout link. The link is always available, so this method is here, in BasePage.
     *
     * @return - LoginPage PageObject.
     */
    public LoginPage logout() {
        Log.logInfo("Logging out.");
        UiHelper.click(BY_LNK_LOGOUT);

        //TODO: waitForPageLoad() might be needed.
        return PageFactory.initElements(Browser.getDriver(), LoginPage.class);
    }

    /**
     * Clicks Home link. If current user is not logged in then LoginPage opens.
     *
     * @return - Just a BasePage PageObject.
     */
    public BasePage tryToGetToHomePage() {
        Log.logInfo("Clicking Home.");
        UiHelper.click(BY_LNK_HOME);

        //TODO: waitForPageLoad() might be needed.
        return new BasePage();
    }
}