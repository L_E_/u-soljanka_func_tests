package le.u_solnka.pages;

import le.u_solnka.utils.entities.TestUser;
import le.u_solnka.utils.helpers.UiHelper;
import org.openqa.selenium.By;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * PageObject class for "CreateUser" page.
 * //TODO: PageFactory (<a href="http://code.google.com/p/selenium/wiki/PageFactory">PageFactory</a>) may be an option.
 */
public class CreateUserPage extends BasePage{
    private static final Logger logger = LoggerFactory.getLogger(CreateUserPage.class);

    /**
     * ById locator for Honorific selector.
     */
    private static final By BY_SLCT_HONORIFIC = new By.ById("honorific");

    /**
     * ById locator for loginName field.
     */
    private static final By BY_TB_LOGINNAME = new By.ById("loginName");

    /**
     * ById locator for firstUserName field.
     */
    private static final By BY_TB_FIRSTNAME = new By.ById("firstUserName");

    /**
     * ById locator for middleUserName field.
     */
    private static final By BY_TB_MIDDLENAME = new By.ById("middleUserName");

    /**
     * ById locator for lastUserName field.
     */
    private static final By BY_TB_LASTNAME = new By.ById("lastUserName");

    /**
     * ById locator for age field.
     */
    private static final By BY_TB_AGE = new By.ById("age");

    /**
     * ById locator for password field.
     */
    private static final By BY_TB_PASSWORD = new By.ById("psswd");

    /**
     * ById locator for birth date field.
     */
    private static final By BY_TB_BIRTHDATE = new By.ById("dateId");

    public CreateUserPage() {
    }

    /**
     * Fills in the form with values from the user object's fields.
     * NOTE: only required fields are supported.
     * TODO: add support for all fields (honorific etc)
     * @param user - TestUser entity to be used as  source.
     */
    public void fillInTheForm(TestUser user){
        sendKeysIfNotNull(BY_TB_LOGINNAME, user.getLoginName());
        sendKeysIfNotNull(BY_TB_FIRSTNAME, user.getFirstUserName());
        sendKeysIfNotNull(BY_TB_LASTNAME, user.getLastUserName());
        sendKeysIfNotNull(BY_TB_PASSWORD, user.getPassword());
        sendKeysIfNotNull(BY_TB_BIRTHDATE, user.getBirthDateAsString());
    }

    private void sendKeysIfNotNull(By locator, String valueToSend) {
        if (null != valueToSend){
            UiHelper.sendKeys(locator, valueToSend);
        }else{
            logger.warn(String.format("Cannot send keys to %s, the value is null", locator));
        }
    }
}